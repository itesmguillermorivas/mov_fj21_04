package com.example.mov_fj21_04;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;

public class RecyclerActivity extends AppCompatActivity implements View.OnClickListener {

    private RecyclerView recyclerView;
    private ArrayList<String> data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler);

        // recycler view is a widget used to display collections
        // data (absolute abstract) - adapter (translator) - view (absolute concrete)
        recyclerView = findViewById(R.id.recyclerView);

        data = new ArrayList<>();
        data.add("Kaiser");
        data.add("Fifi");
        data.add("Fido");
        data.add("Firulais");
        data.add("Killer");
        data.add("Kaiser");
        data.add("Fifi");
        data.add("Fido");
        data.add("Firulais");
        data.add("Killer");
        data.add("Kaiser");
        data.add("Fifi");
        data.add("Fido");
        data.add("Firulais");
        data.add("Killer");
        data.add("Kaiser");
        data.add("Fifi");
        data.add("Fido");
        data.add("Firulais");
        data.add("Killer");

        DoggoAdapter adapter = new DoggoAdapter(data, this);

        // recycler views use a layout manager
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);

        recyclerView.setLayoutManager(llm);
        recyclerView.setAdapter(adapter);

    }

    @Override
    public void onClick(View v) {

        int pos = recyclerView.getChildLayoutPosition(v);
        Toast.makeText(this, data.get(pos), Toast.LENGTH_SHORT).show();
    }
}