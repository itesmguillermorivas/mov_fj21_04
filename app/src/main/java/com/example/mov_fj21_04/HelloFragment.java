package com.example.mov_fj21_04;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link HelloFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HelloFragment extends Fragment {

    private static final String ARG_PARAM1 = "message";

    private String message;
    private Callback observer;

    public HelloFragment() {
        // Required empty public constructor
    }

    public static HelloFragment newInstance(String param1) {
        HelloFragment fragment = new HelloFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            message = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View v = inflater.inflate(R.layout.fragment_hello, container, false);

        TextView textView = v.findViewById(R.id.textView);
        Button button = v.findViewById(R.id.test_button);
        Button button3 = v.findViewById(R.id.button3);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.wtf("TEST FROM THE FRAGMENT", "ITS WORKING!");
            }
        });

        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                observer.doSomethingInActivity("MESSAGE SENT FROM FRAGMENT");
            }
        });

        textView.setText(message);
        return v;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        if(context instanceof Callback){

            observer = (Callback)context;
        } else {

            throw new RuntimeException("YOU MUST IMPLEMENT THE CALLBACK INTERFACE IF YOU WISH TO USE HELLOFRAGMENT");
        }
    }

    public void sayHello(){

        Log.wtf("HELLO","JUST SAYING HELLO");
    }

    public interface Callback {

        public void doSomethingInActivity(String message);
    }
}