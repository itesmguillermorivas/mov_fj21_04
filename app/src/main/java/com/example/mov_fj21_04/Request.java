package com.example.mov_fj21_04;

// we are going to do a thread
// 2 choices -
// extend Thread
// implement interface

import android.os.Message;
import android.os.Handler;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class Request extends Thread {

    private String url;
    private Handler handler;

    public Request(String url, Handler handler){
        this.url = url;
        this.handler = handler;
    }

    // we ovewrite the run method in order to add concurrent code
    public void run(){

        try {

            URL address = new URL(url);
            HttpURLConnection connection = (HttpURLConnection) address.openConnection();

            int code = connection.getResponseCode();
            // 200s - OKs
            // 400s - user error
            // 500s - server error

            if(code == HttpURLConnection.HTTP_OK){

                // retrieve data from the message's body
                InputStream is = connection.getInputStream();

                BufferedReader br = new BufferedReader(new InputStreamReader(is));

                // string builders!
                StringBuilder builder = new StringBuilder();
                String currentLine;

                // = operator save reference/value on variable
                // it actually returns a result!
                // it returns the value being assigned
                int x, y, z;
                x = y = z = 2;

                while((currentLine = br.readLine()) != null){

                    builder.append(currentLine);
                }

                String result = builder.toString();
                Log.wtf("REQUEST", result);

                JSONArray resultArray = new JSONArray(result);

                Message msg = new Message();
                msg.obj = result;

                handler.sendMessage(msg);

            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }
}
