package com.example.mov_fj21_04;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class RequestActivity extends AppCompatActivity implements Handler.Callback {

    private Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request);

        handler = new Handler(Looper.getMainLooper(), this);

        // PUROS EJEMPLOS DE PARSING DE JSON
        String json1 = "{'nombre':'Juan', 'edad':21}";
        String json2 = "{'nombre':'Maria', 'edad':20, 'mascota':{'nombre':'Fifi', 'especie':'Perro'}}";
        String json3 = "{'nombre':'Sofia', 'calificaciones':[95,90,100]}";
        String json4 = "[10,20,30,40]";
        String json5 = "[{'nombre':'A', 'edad':19}, {'nombre':'B', 'edad':20}, {'nombre':'C', 'edad':21}]";

        try {

            JSONObject obj1 = new JSONObject(json1);
            Log.wtf("JSON", obj1.getString("nombre"));
            Log.wtf("JSON", obj1.getInt("edad") + "");

            JSONObject obj2 = new JSONObject(json2);
            Log.wtf("JSON", obj2.getString("nombre"));
            Log.wtf("JSON", obj2.getInt("edad") + "");

            JSONObject obj21 = obj2.getJSONObject("mascota");
            Log.wtf("JSON", obj21.getString("nombre"));
            Log.wtf("JSON", obj21.getString("especie"));

            JSONObject obj3 = new JSONObject(json3);
            Log.wtf("JSON", obj3.getString("nombre"));

            JSONArray obj31 = obj3.getJSONArray("calificaciones");

            for(int i = 0; i < obj31.length(); i++){

                Log.wtf("JSON", obj31.getInt(i) + "");
            }

            JSONArray obj4 = new JSONArray(json4);

            for(int i = 0; i < obj4.length(); i++){

                Log.wtf("JSON", obj4.getInt(i) + "");
            }


            JSONArray obj5 = new JSONArray(json5);

            for(int i = 0; i < obj5.length(); i++){

                Log.wtf("JSON", "______________________________");
                JSONObject obj51 = obj5.getJSONObject(i);
                Log.wtf("JSON", obj51.getString("nombre"));
                Log.wtf("JSON", obj51.getInt("edad") + "");
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void doRequest(View v){

        Request request = new Request("https://api.github.com/users", handler);

        // trigger the start of the thread behaviour
        request.start();
    }

    @Override
    public boolean handleMessage(@NonNull Message msg) {


        String respuesta = msg.obj.toString();
        Toast.makeText(this, "RECIBIDO: " + respuesta, Toast.LENGTH_SHORT).show();

        // parsing de JSON
        try {
            JSONArray dataGithub = new JSONArray(respuesta);
            //JSONArray dataGithub = (JSONArray)msg.obj;

            for(int i = 0; i < dataGithub.length(); i++){

                Log.wtf("JSON", "**********");

                JSONObject temp = dataGithub.getJSONObject(i);
                Log.wtf("JSON", temp.getString("login"));
                Log.wtf("JSON", temp.getInt("id") + "");
                Log.wtf("JSON", temp.getBoolean("site_admin") + "");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return true;
    }
}