package com.example.mov_fj21_04;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements HelloFragment.Callback {

    private static final String TAG_FRAGMENT = "fragment";
    private DoggoFragment doggo;
    private HelloFragment hello;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        doggo = new DoggoFragment();
        hello = HelloFragment.newInstance("HEY EVERYONE! IT'S WORKING!");


        setFragment(doggo);

        Toast.makeText(this, getString(R.string.just_hello), Toast.LENGTH_SHORT).show();
    }

    private void setFragment(Fragment newOne){

        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();

        // within the same transaction whatever amount of actions
        transaction.add(R.id.container, newOne, TAG_FRAGMENT);
        transaction.commit();
    }

    private void removeFragment(){

        FragmentManager manager = getSupportFragmentManager();
        Fragment fragment = manager.findFragmentByTag(TAG_FRAGMENT);
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.remove(fragment);
        transaction.commit();
    }

    public void swapFragments(View v){

        FragmentManager manager = getSupportFragmentManager();
        Fragment fragment = manager.findFragmentByTag(TAG_FRAGMENT);

        removeFragment();

        if(fragment == doggo){
            setFragment(hello);
        } else {
            setFragment(doggo);
        }
    }

    public void sayHello(View v){

        FragmentManager manager = getSupportFragmentManager();
        Fragment fragment = manager.findFragmentByTag(TAG_FRAGMENT);

        if(hello == fragment){

            hello.sayHello();
        } else {

            Toast.makeText(this, "NOT THE RIGHT FRAGMENT!", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void doSomethingInActivity(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    public void changeToRecycler(View v){

        Intent i = new Intent(this, RecyclerActivity.class);
        startActivity(i);
    }

    public void changeToRequest(View v){

        Intent i = new Intent(this, RequestActivity.class);
        startActivity(i);
    }
}