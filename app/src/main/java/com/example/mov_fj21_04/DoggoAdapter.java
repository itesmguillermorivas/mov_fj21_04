package com.example.mov_fj21_04;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class DoggoAdapter extends RecyclerView.Adapter<DoggoAdapter.DoggoViewHolder>{

    private ArrayList<String> data;
    private View.OnClickListener listener;

    public DoggoAdapter(ArrayList<String> data, View.OnClickListener listener){

        this.listener = listener;
        this.data = data;
    }

    @NonNull
    @Override
    public DoggoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = (View) LayoutInflater.from(parent.getContext()).inflate(R.layout.row, parent, false);

        // add behaviour to the button
        // v.findViewById()

        v.setOnClickListener(listener);
        DoggoViewHolder dvh = new DoggoViewHolder(v);
        return dvh;
    }

    @Override
    public void onBindViewHolder(@NonNull DoggoViewHolder holder, int position) {

        holder.rowButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.wtf("BUTTON!", "data on position: " + position + ", " +data.get(position));
            }
        });

        holder.rowText1.setText(data.get(position));
        holder.rowText2.setText(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    // first - define a view holder
    // view holder is a class in charge of dealing with an specific type of view
    public class DoggoViewHolder extends RecyclerView.ViewHolder{

        public TextView rowText1, rowText2;
        public Button rowButton1;

        public DoggoViewHolder(@NonNull View itemView) {
            super(itemView);

            rowText1 = itemView.findViewById(R.id.rowText1);
            rowText2 = itemView.findViewById(R.id.rowText2);
            rowButton1 = itemView.findViewById(R.id.rowButton1);
        }
    }
}
